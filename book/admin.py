# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from book.models import Book

# Register your models here.

admin.site.register(Book)
