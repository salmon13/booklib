from django.forms import ModelForm
from book.models import Book


class BookModelForm(ModelForm):
    class Meta:
        model = Book
        fields = ['title', 'author', 'price']

