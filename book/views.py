# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect

from book. models import Book
from book.forms import BookModelForm

# Create your views here.

def welcome(request):
    return HttpResponse('Welcome to library.')

def display_books(request):
    return render(request, template_name='book/book.html',
           context={'books':Book.objects.all()})

def add_book(request):
    # we should display a form in case of GET
    if request.method == 'GET':
       return render(request, template_name='book/book_form.html',
                    context={'form': BookModelForm()})

    if request.method == 'POST':
        book_form = BookModelForm(request.POST)
        if book_form.is_valid():
            book = book_form.save(commit=False)
            book.save()
        return HttpResponseRedirect(redirect_to=reverse('display_books')) 

def remove_book(request):
    pass

def update_book(request):
    pass

def display_book(request):
    pass
