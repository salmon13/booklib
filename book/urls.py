# book/urls.py

from django.conf.urls import url
from book.views import welcome, display_books, add_book

urlpatterns = [
    url('welcome/', welcome, name='welcome'),
    url('display_books', display_books, name='display_books'),
    url('add_book', add_book, name='add_book')

]
